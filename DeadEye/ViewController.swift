//
//  ViewController.swift
//  DeadEye
//
//  Created by eren on 10.11.2018.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    var currentValueOfSLider = 0
    @IBOutlet weak var lblTargetValue: UILabel!    
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var lblScore: UILabel!
    @IBOutlet weak var lblRoundCount: UILabel!
    var targetValue = 0
    var score = 0
    var roundNo = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //currentValueOfSLider = lroundf(slider.value)
        startNewGame()
        
    }
    
    func startNewGame(){
        score = 0
        roundNo = 0
        startNewRound()
    }
    
    func startNewRound(){
        targetValue = 1 + Int(arc4random_uniform(100))
        currentValueOfSLider = 50
        slider.value = Float(currentValueOfSLider)
        roundNo += 1
        updateLabels()
        
    }
    
    func updateLabels(){
        lblTargetValue.text = "\(targetValue)"
        lblScore.text = String(score)
        lblRoundCount.text = String(roundNo)
    }
    
    @IBAction func sliderMoved(_ slider:UISlider){
//        print("Slider value is : \(slider.value)")
        currentValueOfSLider = lroundf(slider.value)
    }

    @IBAction func clickedHitMe(_ sender: UIButton) {
        let difference = abs(targetValue - currentValueOfSLider)
        var points = 100 - difference
        
        let title: String
        if difference == 0 {
            title = "Perfect!"
            points += 100
        }else if difference < 5 {
            title = "You almost got it!"
            if difference == 1{
                points += 50
            }
        }else if difference < 10{
            title = "Pretty good!"
        }else{
            title = "Come on man focus!"
        }
        score += points
        
        let message = "Your value is \(currentValueOfSLider) must be \(targetValue) \nYour score is \(points)"
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: .default, handler: {
            action in
            self.startNewRound()
        })
//        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
//            UIAlertAction in
//            NSLog("Cancel Pressed")
//        }
        
        // Add the actions
        alertController.addAction(okAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
        //startNewRound()
    }
    
    @IBAction func clickedReset(_ sender: Any) {
       startNewGame()
    }
    
    
}

